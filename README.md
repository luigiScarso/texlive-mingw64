# texlive-mingw64

Mingw win64 programs for texlive.

* `auxprograms.tar.xz`: aux programs for texlive

* `mingw64.tar.xz`: texlive programs. It's the official set for TeXLive 2019

* `mingw64-rev-50902.tar.xz`: texlive from svn repo., revision 50902

* `texlive-mingw.tar.xz`: programs to build texlive

* `harftex:` harftex from <https://github.com/khaledhosny/harftex> (still outside texlive)
---

Assuming `/opt` as working dir, `/opt/texlive-mingw` as the directory 
where the programs to build texlive live, and `/opt/texlive`
the directory with a checkout of the TeXLive svn repo,
to cross-compile  under Ubuntu we can use something like this:

~~~

k=$(pwd)
CPPFLAGS="-I/opt/texlive-mingw/include"
LDFLAGS="-L/opt/texlive-mingw/lib"
#: ${CONFHOST:=--host=x86_64-w64-mingw32}
#: ${CONFBUILD:=--build=x86_64-unknown-linux-gnu}
CONFHOST="--host=x86_64-w64-mingw32"
CONFBUILD="--build=x86_64-unknown-linux-gnu"
RANLIB="${CONFHOST#--host=}-ranlib"
STRIP="${CONFHOST#--host=}-strip"
LDFLAGS="${LDFLAGS} -fno-lto -fno-use-linker-plugin -static-libgcc -static-libstdc++"
export CPPFLAGS CFLAGS CXXFLAGS LDFLAGS LIBS

TEXLIVEOPT="-C --without-x --enable-multiplatform  --disable-xpdfopen --enable-shared -disable-native-texlive-build --enable-xindy CLISP=/opt/texlive-mingw/bin/clisp-2.49-mingw/clisp.exe"
cd ${k}/texlive
./Build  $TEXLIVEOPT $CONFHOST $CONFBUILD  $WARNINGFLAGS 2>&1 |tee  ${k}/OUT-Build-W64
cd ${k}

~~~

The log of building is in `/opt/OUT-Build-W64`